FROM node:12

ENV APP_HOME /usr/src/app
ENV API_PORT 4000

EXPOSE $API_PORT
WORKDIR /$APP_HOME

COPY . $APP_HOME/

RUN npm install

CMD [ "npm", "start" ]

