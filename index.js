const express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    morgan = require('morgan'),
    swaggerUi = require('swagger-ui-express');

const conn = require('./db.js')

const swaggerDocument = require('./swagger.json');

const routes = require('./routes/index');

const port = process.env.PORT || 4000;

let app = express();

app.use(cors({
    origin: `http://localhost:${port}`,
    credentials: true,
}));

//console log request info
app.use(morgan('dev'));

//app utility
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json({extended: false}));

//response headers
app.use((request, response, next) => {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE OPTIONS");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Z-Key");

    if (request.method === "OPTIONS") {
        response.header("Access-Control-Allow-Origin", "*");
        response.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
        response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Z-Key");
        return response.status(200).json({})
    }

    next();
});

//routes
app.use('/api', routes);

//swagger
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//error 404 caching
app.use((request, response, next) => {
    next(JSON.stringify({
        status: 404,
        message: "Not Found"
    }));
});

app.use((error, request, response) => {
    response.status(error.status || 500);
    response.json({
        error: {
            message: error.message
        }
    });
});

conn.then(() => {
    console.log('app ready at port: ' + port);
    app.listen(port);
})
    .catch(error => {
        console.log(error)
    });

module.exports = app
