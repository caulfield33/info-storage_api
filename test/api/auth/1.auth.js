const chai = require('chai');
const request = require('supertest');

const app = require("../../../index.js");
const conn = require("./../../../db.js");

const testData = new Date().toISOString();
const newUser = {
    "email": `${testData}@test.test`,
    "photoURL": testData,
    "name": testData,
    "password": testData
}

describe("Auth [/api/v1/auth]", () => {
    before((done) => {
        conn
            .then(() => done())
            .catch((e) => done(e))
    })

    beforeEach(function (done) {
        setTimeout(function(){
            done();
        }, 500);
    });

    it(`Creating new user`, (done) => {
        request(app).post('/api/v1/auth/create-user').send(newUser)
            .then((res) => {
                chai.expect(res.body).to.contain.property('token')
                chai.expect(res.body).to.contain.property('refreshToken')

                chai.expect(res.body.user.email).to.be.equal(newUser.email)

                done()
            })
            .catch((e) => {
                done(e)
            })


    })


    it('Login new user', (done) => {
        request(app).post('/api/v1/auth/login').send({email: newUser.email, password: newUser.password})
            .then((res) => {
                chai.expect(res.body).to.contain.property('token')
                chai.expect(res.body).to.contain.property('refreshToken')

                newUser.token = res.body.token
                newUser.refreshToken = res.body.refreshToken
                newUser._id = res.body.user._id

                chai.expect(res.body.user.email).to.be.equal(newUser.email)

                done()
            })
            .catch((e) => {
                done(e)
            })
    })


    it('Refresh Token', (done) => {
        request(app).post('/api/v1/auth/refresh-token').send({userId: newUser._id, token: newUser.refreshToken})
            .then((res) => {
                chai.expect(res.body).to.contain.property('token')
                chai.expect(res.body).to.contain.property('tokenExpiration')

                done()
            })
            .catch((e) => {
                done(e)
            })
    })

    it('Logout', (done) => {
        request(app).post('/api/v1/auth/logout').send({userId: newUser._id})
            .then((res) => {
                chai.expect(res.body).to.contain.property('logout')

                chai.expect(res.body.logout).to.be.equal(true);
                done()
            })
            .catch((e) => {
                done(e)
            })
    })


})