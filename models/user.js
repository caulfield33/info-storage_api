const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
    email: {
        type: String,
        trim: true,
        unique: true,
        required: true
    },

    refreshToken: {
        type: String,
        default: null
    },

    name: {
        type: String,
        required: true
    },

    photoURL: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    createdCategories: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Category'
        }
    ],

    createdRecords: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Record'
        }
    ]
}, {timestamps: true});

module.exports = mongoose.model('User', userSchema);
