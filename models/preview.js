const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const previewSchema = new Schema({
    siteName: {
        type: String
    },
    
    youtube: {
        type: String
    },

    url: {
        type: String
    },

    title: {
        type: String
    },

    ogVideoUrl: {
        type: String
    },

    ogUrl: {
        type: String
    },

    imageWidth: {
        type: String
    },

    imageType: {
        type: String
    },

    imageHeight: {
        type: String
    },

    image: {
        type: String
    },

    description: {
        type: String
    },
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {timestamps: true});

module.exports = mongoose.model('Preview', previewSchema);