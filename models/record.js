const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const recordSchema = new Schema({
    title: {
        type: String,
        required: true
    },

    link: {
        type: String,
        required: true
    },

    description: {
        type: String
    },

    status: {
        type: String,
        default: 'private'
    },

    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },

    preview: {
        type: Schema.Types.ObjectId,
        ref: 'Preview'
    },

    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
}, {timestamps: true});

module.exports = mongoose.model('Record', recordSchema);