const router = require('express').Router(),
      auth   = require('../../middleware/isAuth'),
      lp     = require('../../controllers/link-preview');

router.post('/', auth, lp.getLinkPreview);

module.exports = router;
