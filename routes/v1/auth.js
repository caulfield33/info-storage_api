let router = require('express').Router();

const Auth = require('../../controllers/auth');

router.post('/login',          Auth.login);

router.post('/create-user',    Auth.createUser);

router.post('/refresh-token',  Auth.refreshToken);

router.post('/logout',         Auth.logout);

module.exports = router;
