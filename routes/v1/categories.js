let router      = require('express').Router();

const auth      = require('./../../middleware/isAuth');

const Category  = require('../../controllers/category');

router.get('/getCategories', auth, Category.getCategories);


module.exports = router;