let router   = require('express').Router();

const auth   = require('./../../middleware/isAuth');

const Record = require('../../controllers/record');

router.get('/getRecords',       auth, Record.getRecords);

router.delete('/deleteRecord',  auth, Record.deleteRecord);

router.put('/updateRecord',     auth, Record.updateRecord);

router.post('/createRecord',    auth, Record.createRecord);

module.exports = router;