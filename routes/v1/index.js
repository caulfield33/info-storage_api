let router = require('express').Router();

router.use('/link-preview', require('./link-preview'));

router.use('/categories',   require('./categories'));

router.use('/auth',         require('./auth'));

router.use('/records',      require('./records'));

router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;