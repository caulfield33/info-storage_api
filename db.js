const mongoose = require('mongoose');


const connection = `mongodb://${process.env.DATABASE_URL}:${process.env.DATABASE_PORT}/${process.env.DATABASE_COLLECTION}`;

module.exports = mongoose.connect(
    connection,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        createIndexes: true
    }
);