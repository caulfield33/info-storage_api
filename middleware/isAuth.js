const jwt = require('jsonwebtoken');

module.exports = (request, response, next) => {
    try {
        const token = request.headers.authorization.split(" ")[1];

        request.userData = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET || "ACCESS_TOKEN_SECRET");
        next();
    } catch (error) {
        return response.status(401).json({
            msg: 'Auth fail'
        });
    }
};
