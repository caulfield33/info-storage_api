const User   = require('../models/user');
const {createAccessToken, createRefreshToken} = require('../helpers/tokenService');
const { verify } = require('jsonwebtoken');
const  bcrypt = require('bcryptjs');

module.exports = {

    createUser: async (request, response) => {
        try {
            const existingUser = await User.findOne({ email: request.body.email });

            if (existingUser) {
                throw new Error('User exists already.');
            }

            const hashedPassword = await bcrypt.hash(request.body.password, 12);

            const user = new User({
                email: request.body.email,
                photoURL: request.body.photoURL,
                name: request.body.name,
                password: hashedPassword,
            });

            const result = await user.save();

            const token = await createAccessToken(result._id, result.email);
            const refreshToken = await createRefreshToken(result._id, result.email);

            result.refreshToken = refreshToken;

            await result.save();

            return response.status(201).json({
                token,
                refreshToken,
                user: {
                    email: result.email,
                    _id: result._id,
                    name: result.name,
                    photoURL: result.photoURL,
                    createdAt: result.createdAt,
                    updatedAt: result.updatedAt
                },
                tokenExpiration: '10m',
                refreshTokenExpiration: '28d',
                requestData: {
                    method: "POST",
                }
            });
        } catch (err) {
            console.log(err);
            response.status(404).json({
                error: err
            });
        }
    },

    login: async (request, response) =>  {

        const user = await User.findOne({email: request.body.email});

        if (!user) {
            return response.status(409).json({
                msg: "User not exist."
            })
        }

        const isEqual = await bcrypt.compare(request.body.password, user.password);

        if (!isEqual) {
            return response.status(409).json({
                msg: "Auth fails"
            })
        }

        const token = await createAccessToken(user._id, user.email);
        const refreshToken = await createRefreshToken(user._id, user.email);

        user.refreshToken = refreshToken;

        await user.save();

        return response.status(200).json({
            token,
            refreshToken,
            user: {
                email: user.email,
                _id: user._id,
                name: user.name,
                photoURL: user.photoURL,
                createdAt: user.createdAt,
                updatedAt: user.updatedAt
            },
            tokenExpiration: '10m',
            refreshTokenExpiration: '28d',
            requestData: {
                method: "POST",
            },
        });
    },


    logout: async (request, response) => {
        const {userId} = request.body;

        const user = await User.findById(userId);

        user.refreshToken = null;

        await user.save();

        return response.status(201).json({
            logout: true
        });

    },

    refreshToken: async (request, response) => {
        const {userId, token} = request.body;

        if (!token) {
            return response.status(401).json({
                msg: 'Auth fail'
            });
        }

        try {
            verify(token, process.env.REFRESH_TOKEN_SECRET || "REFRESH_TOKEN_SECRET");
        } catch (err) {
            return response.status(401).json({
                msg: 'Auth fail'
            });
        }

        const user = await User.findById(userId);


        if (!user) {
            return response.status(401).json({
                msg: 'Auth fail'
            });
        }

        if (user.refreshToken !== token) {
            return response.status(401).json({
                msg: 'Auth fail'
            });
        }

        const newToken = createAccessToken(user._id, user.email);

        await user.save();

        return response.status(201).json({
            token: newToken,
            tokenExpiration: '10m'
        });
    }
};
