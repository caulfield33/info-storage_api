const lp = require('../config/link-previw');

module.exports = {

    getLinkPreview: async (request, response)  => {

        try {
            const linkInfo = await lp(request.body.url);

            response.status(200).json({
                requestData : {
                    method: "GET",
                },
                result: linkInfo
            })
        } catch (err) {
            console.log(err);
            response.status(404).json({
                error: err
            });
        }

    }
};