const Record    = require('../models/record');
const Category  = require('../models/category');
const Preview   = require('../models/preview');
const User      = require('../models/user');

const lp        = require('../config/link-previw');

module.exports = {

    getRecords: async (request, response) => {
        try {
            const userId = request.query.userId;
            const queryText = request.query.queryText;
            const queryCategory = request.query.queryCategory;

            let dbQuery =  { creator: userId };

            if (queryText) {
                dbQuery['title'] = { $regex: '.*' + queryText + '.*' };
            }
            if (queryCategory){
                dbQuery['category'] = queryCategory;
            }

            const dbRecords = await Record.find(dbQuery)
                .populate('preview')
                .populate('category')
                .skip(request.query.offset ? Number(request.query.offset) : 0)
                .limit(request.query.limit ? Number(request.query.limit) : 0);

            const total = await Record.countDocuments(dbQuery).exec();

            response.status(200).json({
                requestData : {
                    method: "GET",
                },
                result: dbRecords,
                total: total
            })
        } catch (err) {
            console.log(err);
            response.status(404).json({
                error: err
            });
        }
    },

    updateRecord: async (request, response)  => {
        try {
            if (request.body._id === undefined) {
                throw new Error('Record not found.');
            }

            const userId = request.query.userId;

            const record = await Record.findById(request.body._id)
                .populate('preview')
                .populate('category');

            let promises = [];

            let updatedPreview;
            if (request.body.link && record.link !== request.body.link) {

                const linkPreview = await lp(request.body.link);

                updatedPreview = new Preview({
                    ...linkPreview,
                    creator: userId
                });

                await Preview.deleteOne({ _id: record.preview._id });

                await updatedPreview.save();
            }


            let isNewCategory = false;
            if (request.body.category && String(request.body.category._id) !== String(record.category._id)) {

                let newCategory;
                let oldCategory;

                if (request.body.category === undefined || request.body.category._id === undefined) {
                    newCategory = new Category({
                        title: request.body.category.title,
                        count: 1,
                        creator: userId
                    });

                    if (record.category._id) {
                        oldCategory = await Category.findById(record.category._id);
                        if (oldCategory.count !== 0) {
                            oldCategory.count = oldCategory.count - 1;
                        }
                        promises.push(oldCategory.save());
                    }

                    promises.push(newCategory.save());
                }

                isNewCategory = true;
            }

            promises = await Promise.all(promises);

            const updatedRecord = Object.assign(record, {
                ...request.body,
                preview: updatedPreview ? updatedPreview : record.preview._id,
                category: isNewCategory ? promises[promises.length -1]['_id'] : record.category._id
            });

            const result = await updatedRecord.save().then(t => {
               return t.populate('preview').populate('category').execPopulate();
            });

            response.status(201).json({
                requestData : {
                    method: "PUT",
                },
                result: {
                    ...result._doc
                }
            });

            return result
        } catch (err) {
            console.log(err);
            response.status(500).json({
                msg: 'Updating filed.',
                error: err
            });
        }
    },

    deleteRecord: async (request, response) => {
        try {
            const record = await Record.findOne({ _id: request.query.recordId });

            const promises = [
                Record.deleteOne({ _id: request.body.recordId }),
                Preview.deleteOne({ _id: record.preview }),
            ];

            await Record.count({category: record.category}, (err, count) => {
                if (count === 1) {
                    promises.push(Category.deleteOne({ _id: record.preview }))
                }
            });

            await Promise.all(promises);

            response.status(201).json({
                msg: `Record: ${record._id}. Was deleted`,
                requestData: {
                    method: "DELETE",
                }
            })

        } catch (err) {
            console.log(err);
            response.status(500).json({
                msg: 'Deleting filed.',
                error: error
            })
        }

    },

    createRecord: async (request, response) => {

        try {
            const userId = request.query.userId;

            const creator = await User.findById(userId);

            if (!creator) {
                throw new Error('User not found.');
            }

            const promises = [];
            let isNewCategory = false;

            let category;
            if (request.body.category === undefined || request.body.category._id === undefined) {
                category = new Category({
                    title: request.body.category.title,
                    creator: userId
                });
                isNewCategory = true;
            } else {
                category = await Category.findById(request.body.category._id);

            }
            promises.push(category.save());


            let preview;
            if (request.body.preview === undefined) {
                const linkPreview = await lp(request.body.link);

                preview = new Preview({
                    ...linkPreview,
                    creator: userId
                })
            } else {
                preview = new Preview({
                    description: request.body.preview.description   || null,
                    image:       request.body.preview.image         || null,
                    imageHeight: request.body.preview.imageHeight   || null,
                    imageType:   request.body.preview.imageType     || null,
                    imageWidth:  request.body.preview.imageWidth    || null,
                    ogUrl:       request.body.preview.ogUrl         || null,
                    ogVideoUrl:  request.body.preview.ogVideoUrl    || null,
                    siteName:    request.body.preview.siteName      || null,
                    title:       request.body.preview.title         || null,
                    url:         request.body.preview.url           || null,
                    youtube:     request.body.preview.youtube       || null,
                    creator:     userId
                });
            }
            promises.push(preview.save());

            const data = await Promise.all(promises);

            const record = new Record({
                creator:        userId,
                preview:        data[1]._id,
                link:           request.body.link,
                title:          request.body.title,
                description:    request.body.description,
                category:       data[0]._id
            });

            let createdRecord = await record.save();

            creator.createdRecords.push(record);

            if(isNewCategory) {
                creator.createdCategories.push(data[0]);
            }

            await creator.save();

            response.status(201).json({
                requestData : {
                    method: "POST",
                },
                result: {
                    ...createdRecord._doc,
                    preview:  data[1],
                    category: data[0]
                }
            })

        } catch (err) {
            console.log(err);
            response.status(500).json({
                msg: 'Creating filed.',
                error: err
            });
        }
    },
};
