const Category = require("../models/category");
const Record   = require('../models/record');

module.exports = {

    getCategories: async (request, response) => {
        try {
            const userId = request.query.userId;

            const queryText = request.query.queryText;

            let dbQuery =  { creator: userId };

            if (queryText) {
                dbQuery['title'] = { $regex: '.*' + queryText + '.*' };
            }

            if (!userId) {
                response.status(404).json({
                    error: "No user ID"
                });

                throw new Error('No user ID')
            }

            const categories = await Category.find(dbQuery)
                .skip(request.query.offset ? Number(request.query.offset) : 0)
                .limit(request.query.limit ? Number(request.query.limit) : 0);

            let promises = [];

            categories.forEach(c => {
                promises.push(Record.countDocuments({ category: c._id }).exec())
            });

            const total = await Category.countDocuments(dbQuery).exec();

            const count = await Promise.all(promises);

            for (let i = 0; i < categories.length; i++) {
                categories[i].count = count[i]
            }

            response.status(200).json({
                requestData : {
                    method: "GET",
                },
                result: categories,
                total: total
            })

        } catch (err) {
            console.log(err);
            response.status(404).json({
                error: err
            })
        }
    }

};