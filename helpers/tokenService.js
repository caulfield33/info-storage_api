const jwt    = require('jsonwebtoken');

module.exports = {
    createAccessToken: (id, email) => {
        return jwt.sign(
            {userId: id, email},
            process.env.ACCESS_TOKEN_SECRET || "ACCESS_TOKEN_SECRET",
            {expiresIn: '10m'}
        );
    },

    createRefreshToken: (id, email) => {
        return jwt.sign(
            {userId: id, email},
            process.env.REFRESH_TOKEN_SECRET || "REFRESH_TOKEN_SECRET",
            {expiresIn: '28d'}
        );
    },
};
